# ProblemataRestApi (module pour Omeka S)


Module [Omeka S](https://github.com/omeka/omeka-s) qui augmente la REST API pour les besoins spécifiques de Problemata. Voir [le modèle de donnée](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md).


**Articles**

- affiche le display_title des lignes sur "pb:item_set"
- affichage les traductions (dcterms:hasVersion) avec leur langue sur "pb:hasVersion"
- affichage de la traduction source (dcterms:isVersion) sur "pb:isVersion"

**Agents**

- affiche les lignes d'un agent sur "pb:item_set"
- affiche les articles d'un agent sur "pb:Text"

**Lines**

- affiche le nombre d'auteurs sur "pb:creatorcount"
- affiche les objets auteurs (dcterms:creator) sur "pb:creator"
- affiche les articles d'une ligne sur "pb:Text"

**Resource**

- affiche les médias d'une ressource sur "pb:media"

Options commentées :

- affiche les articles d'une ressource sur "pb:Text"
- affiche les Collections d'une ressource sur "pb:CollectedDocument"
- affiche les Institions d'une ressource sur "pb:Organization"


---

Inspiré par :
- [Mapping](https://github.com/omeka-s-modules/Mapping/)
- [ApiInfo](https://github.com/Daniel-KM/Omeka-S-module-ApiInfo)


## Installation

- [Voir le manuel](http://omeka.org/s/docs/user-manual/install/)
- nom du dossier : `ProblemataRestApi`