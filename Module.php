<?php
namespace ProblemataRestApi;

use Doctrine\ORM\QueryBuilder;
use Omeka\Module\AbstractModule;
use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;
use Laminas\Mvc\MvcEvent;

class Module extends AbstractModule
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        parent::onBootstrap($event);
    }


    public function attachListeners(SharedEventManagerInterface $sharedEventManager)
    {
        $sharedEventManager->attach(
            'Omeka\Api\Representation\ItemRepresentation',
            'rep.resource.json',
            [$this, 'filterItemJsonLd']
        );
        $sharedEventManager->attach(
            'Omeka\Api\Representation\ItemSetRepresentation',
            'rep.resource.json',
            [$this, 'filterItemSetJsonLd']
        );
    }



    /**
     * Add the mapping and marker data to the item JSON-LD.
     *
     * Event $event
     */
    public function filterItemJsonLd(Event $event)
    {
        $item = $event->getTarget();
        $jsonLd = $event->getParam('jsonLd');
        $api = $this->getServiceLocator()->get('Omeka\ApiManager');

        // cas d'un "dctype:Text" (Article)
        if ("dctype:Text" == $item->getJsonLdType()[1]) {
            // boucle pr afficher le display_title des item-sets (lignes) pr les items (articles)
            $itemSets = [];
            foreach ($item->itemSets() as $itemSetRepresentation) {
                // $obj = $itemSetRepresentation->getReference();
                $obj = [
                    'type' => 'resource',
                    '@id' => $itemSetRepresentation->apiUrl(),
                    'value_resource_id' => $itemSetRepresentation->id(),
                    'display_title' => $itemSetRepresentation->title(),
                ];
                $itemSets[] = $obj;
            }
            // envoi des display_title des item_sets (lignes) à l'API
            $jsonLd['pb:item_set'] = $itemSets;

            // affichage des langues des dcterms:hasVersion (Traduction) de cet item (Article)
            $item_hasVersion = $item->value('dcterms:hasVersion', ['all' => true]);
            if ($item_hasVersion) {
                $obj_hasVersion = [];
                foreach ($item_hasVersion as $version) {
                    $resource = $version->valueResource();
                    $lang = $resource->value('dcterms:language')->value();
                    $obj = [
                        'type' => 'resource',
                        '@id' => $resource->apiUrl(),
                        'value_resource_id' => $resource->id(),
                        'display_title' => $resource->title(),
                        'lang' => $resource->value('dcterms:language')->value(),
                    ];
                    $obj_hasVersion[] = $obj;
                }
                $jsonLd['pb:hasVersion'] = $obj_hasVersion;
            }

            // affichage des langues des dcterms:isVersionOf (Est une traduction) de cet item (Article)
            $item_isVersionOf = $item->value('dcterms:isVersionOf', ['all' => true]);
            if ($item_isVersionOf) {
                $obj_isVersionOf = [];
                foreach ($item_isVersionOf as $version) {
                    $resource = $version->valueResource();
                    $lang = $resource->value('dcterms:language')->value();
                    $obj = [
                        'type' => 'resource',
                        '@id' => $resource->apiUrl(),
                        'value_resource_id' => $resource->id(),
                        'display_title' => $resource->title(),
                        'lang' => $resource->value('dcterms:language')->value(),
                    ];
                    $obj_isVersionOf[] = $obj;
                }
                $jsonLd['pb:isVersionOf'] = $obj_isVersionOf;
            }

            /* MEDIA */
            $article_media_array = [];
            // récup des media de l'item (article)
            $article_medias = $item->media();
            foreach ($article_medias as $article_media) {
                $obj = [
                    'type' => 'o:Media',
                    'o:media_type' => $article_media->mediaType(),
                    '@id' => $article_media->apiUrl(),
                    'o:id' => $article_media->id(),
                    'o:title' => $article_media->title(),
                    'o:thumbnail_urls' => $article_media->thumbnailUrls(),
                    'o:source' => $article_media->source(),
                    'o:filename' => $article_media->filename(),
                ];
                $article_media_array[] = $obj;
            }
            // envoi des medias à l'API
            $jsonLd['pb:media'] = $article_media_array;
        }

        // cas d'un "foaf:Agent" (Agent)
        if ("foaf:Agent" == $item->getJsonLdType()[1]) {
            // requête API pr récup tous les items "dctype:Text" (articles) écrits par un agent
            $response_articles = $api->search('items', [
                'resource_class_label' => 'Text',
                'property' => [
                    [
                        'joiner' => 'and',
                        'property' => 'dcterms:creator',
                        'type' => 'res',
                        'text' => $item->id(),
                    ],
                ],
            ]);
            // $count_articles = $response_articles->getTotalResults();
            $agent_articles = [];
            $agent_lines = [];
            foreach ($response_articles->getContent() as $item) {
                /* recup de tous les articles de cet agent */
                $obj = [
                    'type' => 'resource',
                    '@id' => $item->apiUrl(),
                    'value_resource_id' => $item->id(),
                    'display_title' => $item->title(),
                ];
                // ajout article sans les traductions
                $isVersionOf = (count($item->value('dcterms:isVersionOf', ['all' => true]))) ? true : false;
                if (!$isVersionOf) {
                    $agent_articles[] = $obj;
                }
                
                /* recup de toutes les lignes de cet agent */
                foreach ($item->itemSets() as $item_set) {
                    $obj = [
                        'type' => 'resource',
                        '@id' => $item_set->apiUrl(),
                        'value_resource_id' => $item_set->id(),
                        'display_title' => $item_set->title(),
                    ];
                    $agent_lines[] = $obj;
                }
            }
            
            // envoi de la liste des lignes de cet agent à l'API
            $uniq_agent_lines = unique_multidim_array($agent_lines, 'value_resource_id'); 
            $jsonLd['pb:item_set'] = $uniq_agent_lines;

            // envoi de la liste des articles à l'API
            $jsonLd['pb:Text'] = $agent_articles;
            // $jsonLd['pb_articlecount'] = $count_articles;
        }

        // https://omeka.org/s/docs/developer/api/representations/

        // cas d'un "bibo:Document" (Resource)
        if ("bibo:Document" == $item->getJsonLdType()[1]) {
            $ressource = $item;

            /* MEDIA */
            $media_array = [];
            // récup des media de l'item (resource)
            $medias = $item->media();
            foreach ($medias as $media) {
                $obj = [
                    'type' => 'o:Media',
                    'o:media_type' => $media->mediaType(),
                    '@id' => $media->apiUrl(),
                    'o:id' => $media->id(),
                    'o:title' => $media->title(),
                    'o:thumbnail_urls' => $media->thumbnailUrls(),
                    'o:source' => $media->source(),
                    'o:filename' => $media->filename(),
                ];
                $media_array[] = $obj;
            }
            // envoi des medias à l'API
            $jsonLd['pb:media'] = $media_array;

            /* DCTERMS:RELATION */
            /*
            // requête API pr récup tous les items "dctype:Text" (articles) qui pointent vers cette ressource
            $response_articles = $api->search('items', [
                'resource_class_label' => 'Text',
                'property' => [
                    [
                        'joiner' => 'and',
                        'property' => 'dcterms:relation',
                        'type' => 'res',
                        'text' => $item->id(),
                    ],
                ],
            ]);
            
            $resource_articles = [];
            foreach ($response_articles->getContent() as $item) {
                // recup de tous les articles de cette ressource
                $obj = [
                    'type' => 'resource',
                    '@id' => $item->apiUrl(),
                    'value_resource_id' => $item->id(),
                    'display_title' => $item->title(),
                ];
                $resource_articles[] = $obj;
            }
            // envoi des articles de cette ressource à l'API
            $jsonLd['pb:Text'] = $resource_articles;
            */


            /* LES COLLECTIONS DE CE bibo:Document */
            // requête API pr récup tous les items "bibo:Collected Document" (Collection) qui pointent vers cette bibo:Document
            /*
            $response_collections = $api->search('items', [
                'resource_class_label' => 'Collected Document',
                'property' => [
                    [
                        'joiner' => 'and',
                        'property' => 'dcterms:relation',
                        'type' => 'res',
                        'text' => $ressource->id(),
                    ],
                ],
            ]);
            // création d'un obj pr toutes les collections pointant vers ce bibo:Document
            $list_collections = [];
            foreach ($response_collections->getContent() as $item) {
                $obj = [
                    'type' => 'resource',
                    '@id' => $item->apiUrl(),
                    'value_resource_id' => $item->id(),
                    'display_title' => $item->title(),
                ];
                $list_collections[] = $obj;
            }
            // envoi des collection de ce bibo:Document à l'API
            $jsonLd['pb:CollectedDocument'] = $list_collections;
            */


            /* LES INSTITUTIONS DE CE bibo:Document */
            /*
            // requête API pr récup tous les items "bibo:Organization" (Institution) qui pointent vers cette bibo:Document
            $response_institutions = $api->search('items', [
                'resource_class_label' => 'Organization',
                'property' => [
                    [
                        'joiner' => 'and',
                        'property' => 'dcterms:relation',
                        'type' => 'res',
                        'text' => $ressource->id(),
                    ],
                ],
            ]);
            // création d'un obj pr toutes les collections pointant vers ce bibo:Document
            $list_institutions = [];
            foreach ($response_institutions->getContent() as $item) {
                $obj = [
                    'type' => 'resource',
                    '@id' => $item->apiUrl(),
                    'value_resource_id' => $item->id(),
                    'display_title' => $item->title(),
                ];
                $list_institutions[] = $obj;
            }
            // envoi des Instutions de ce bibo:Document à l'API
            $jsonLd['pb:Organization'] = $list_institutions;
            */
        }

        $event->setParam('jsonLd', $jsonLd);
    }

    /**
     * Custom résultats API pour les item_sets (lignes)
     * notamment "pb_creatorcount" et "pb_creator"
     *
     * Event $event
     */
    public function filterItemSetJsonLd(Event $event)
    {
        $itemset = $event->getTarget();
        $jsonLd = $event->getParam('jsonLd');
        $api = $this->getServiceLocator()->get('Omeka\ApiManager');
        
        // a faire : ajouter en option les class des obj pr limiter la requête
        // get de tous les items (articles) qui sont ds cette item_set (ligne)
        $response = $api->search('items', ['item_set_id' => $itemset->id()]); // search($resource, $data, $options)

        // parcourt de la réponse pr stocker les dc:creator de types "resource" ou "literal"
        $creator_resources = [];
        $creator_literals = [];
        foreach ($response->getContent() as $item) {
            // récup des créators de l'item (article)
            $creators = $item->value('dcterms:creator', ['all' => true]);
            if (!empty($creators)) {
                foreach ($creators as $creator) {
                    // toggle entre "resource" et "literal"
                    if ('resource:item' == $creator->type()) {
                        $creator_json = $creator->jsonSerialize();
                        $creator_obj = [
                            'type' => 'resource',
                            '@id' => $creator_json['@id'],
                            'value_resource_id' => $creator_json['value_resource_id'],
                            'display_title' => $creator_json['display_title'],
                        ];
                        $creator_resources[] = $creator_obj;
                    } else if ('literal' == $creator->type()) {
                        $creator_literals[] = $creator;
                    }
                }
            }
        }

        // array uniq pr les objets dc:creator (agents)
        $uniq_creator_resources = unique_multidim_array($creator_resources, 'value_resource_id'); 
        $uniq_creator_literals = array_unique($creator_literals);

        // calcul du nb de dc:creator (agents)
        $count_creator_resources = count($uniq_creator_resources);
        $count_creator_literals = count($uniq_creator_literals);
        $count_total = $count_creator_resources+$count_creator_literals;

        // envoi du nb de dc:creator (agents) à l'API
        $jsonLd['pb:creatorcount'] = $count_total;
        // envoi des objets resources et literals dc:creator (agents) à l'API
        $jsonLd['pb:creator'] = array_merge($uniq_creator_resources, $uniq_creator_literals);

        /* recup de tous les articles dans cette ligne */
        $line_articles = [];
        foreach ($response->getContent() as $item) {
            // ajout article sans les traductions
            $isVersionOf = (count($item->value('dcterms:isVersionOf', ['all' => true]))) ? true : false;
            if (!$isVersionOf) {
                $obj = [
                    'type' => 'resource',
                    '@id' => $item->apiUrl(),
                    'value_resource_id' => $item->id(),
                    'display_title' => $item->title(),
                ];
                $line_articles[] = $obj;
            }
        }

        // envoi de la liste des articles à l'API
        $jsonLd['pb:Text'] = $line_articles;

        $event->setParam('jsonLd', $jsonLd);
    }

}

/*
    Renvoie un tableau uniq à partir d'un tableau et de la key à comparer
    https://www.php.net/manual/en/function.array-unique.php#116302
*/
function unique_multidim_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();
   
    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return array_values($temp_array);
} 